<?php

namespace Drupal\amplitude\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Amplitude event entities.
 */
interface AmplitudeEventInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
