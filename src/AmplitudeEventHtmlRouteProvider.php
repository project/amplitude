<?php

namespace Drupal\amplitude;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Amplitude event entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class AmplitudeEventHtmlRouteProvider extends AdminHtmlRouteProvider {

}
