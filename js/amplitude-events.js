(function ($, settings) {

  function initEvents() {
    var events = drupalSettings.amplitude.events;

    if (!events) {
      return;
    }

    events.forEach(function (event) {
      switch (event['event_trigger']) {
        case 'pageLoad':
          triggerEvent(event);
          break;

        case 'other':
          triggerEventOn(event, event['event_trigger_other']);
          break;

        default:
          triggerEventOn(event, event['event_trigger']);
          break;
      }
    });
  }

  // Triggers an Amplitude event.
  function triggerEvent(event, elem) {
    var amplitude_instance = amplitude.getInstance();
    var event_name = event['name'];
    var event_properties = event['properties'];

    if (!event_properties) {
      amplitude_instance.logEvent(event_name);
      triggerEventDebug(event_name);
      return;
    }

    var event_properties_parsed = JSON.parse(event_properties);

    if (elem) {
      var event_trigger_data_capture = event['event_trigger_data_capture'];
      var event_trigger_data_capture_properties = event['event_trigger_data_capture_properties'];
      if(event_trigger_data_capture == 1 && event_trigger_data_capture_properties) {
        var event_trigger_data_capture_properties_parsed = JSON.parse(event_trigger_data_capture_properties);
        for(var prop in event_trigger_data_capture_properties_parsed) {
          var selector = event_trigger_data_capture_properties_parsed[prop];
          event_properties_parsed[prop] = selector ? $(elem).find(selector).text() : $(elem).text();
        }
      }
    }

    amplitude_instance.logEvent(event_name, event_properties_parsed);
    triggerEventDebug(event_name, event_properties_parsed);

  }

  // Triggers an Amplitude event on a given user event.
  function triggerEventOn(event, trigger) {
    $(event['event_trigger_selector']).on(trigger, function () {
      triggerEvent(event, $(this));
    });
  }

  // Logs event debug.
  function triggerEventDebug(event_name, event_properties_parsed){

    if (!settings.amplitude.debug) {
      return;
    }

    console.log('Triggered event ' + event_name);

    if (event_properties_parsed) {
      console.log('With properties:');
      console.log(event_properties_parsed);
    }
  }
  initEvents();

})(jQuery, drupalSettings);
